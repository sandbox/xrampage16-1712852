<?php

/**
 * @file
 * Administration settings for Facebook Share integration
 *  Written by Cameron Fillers (cameron@komputerking.com)
 */


/**
 * Admin settings form
 */
function facebook_share_admin_settings() {
	$form = array();
	
	global $base_path;
	
	$path_to_theme = drupal_get_path('module', 'facebook_share');
	
	$form['facebook_share_time_until_update'] = array(
		'#type' => 'textfield',
    '#title' => t('Facebook Share Cache Duration in Seconds'),
    '#default_value' => variable_get('facebook_share_time_until_update', 3600),
    '#description' => t('The amount of time until your website contacts facebook.com for share/like updates in seconds. Default is 1 hour. 
    	Remember that when the share button is pressed, the url will be updated already. These are for passive updates to increase site performance'),
	);
	$form['facebook_share_counter_type'] = array(
		'#type' => 'select',
		'#title' => t('Facebook Share Counter to Display'),
		'#default_value' => variable_get('facebook_share_counter_type', 'share_count'),
		'#options' => array(
			'share_count' => t('Share Counter'),
			'like_count' => t('Like Counter'),
			'comment_count' => t('Comment Counter'),
			'total_count' => t('Total Counter'),
			'click_count' => t('Click Counter'),
		),
		'#definition' => t('When the facebook api is called, more than just the share counter is retreived. As a result, this module will store the counts for all the various
			feedback available. You are able to display any of the counters that you prefer, but we recommend that you display the actual <b>Share Counter</b> that Facebook stores.'),
	);
	$form['facebook_share_display_type'] = array(
		'#type' => 'radios',
		'#title' => t('Default Facebook Share button'),
		'#default_value' => variable_get('facebook_share_display_type', 'box_count'),
		'#options' => array(
			'box_count' => t('<img src="@basepath@pathtotheme/images/@image" />', array('@basepath' => $base_path, '@pathtotheme' => $path_to_theme, '@image' => 'facebook-share-vertical-full-sample.png')),
			'horizontal' => t('<img src="@basepath@pathtotheme/images/@image" />', array('@basepath' => $base_path, '@pathtotheme' => $path_to_theme, '@image' => 'facebook-share-horizontal-full-sample.png')),
			'plain' => t('<img src="@basepath@pathtotheme/images/@image" />', array('@basepath' => $base_path, '@pathtotheme' => $path_to_theme, '@image' => 'facebook-share-collapsed.png')),
			'icon' => t('<img src="@basepath@pathtotheme/images/@image" />', array('@basepath' => $base_path, '@pathtotheme' => $path_to_theme, '@image' => 'facebook-share-icon-32.png')),
		),
	);
	return system_settings_form($form);
}

