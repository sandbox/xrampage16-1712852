function facebookShareUrl(fsid) {
	var facebookShare = '';
	//check to see if the url is available from the information stored in Drupal.settings
	if (Drupal.settings.facebookShare.length > 0) {
		$.each(Drupal.settings.facebookShare, function(index, value) {
			if (value[fsid]) {
				facebookShare = value[fsid];
			}
		});
	}
	
	if (!facebookShare) {
		//create the url to pull the url from the server via json
		var facebookShare = location.protocol+'//'+location.host+'/facebook-share/'+fsid;
		//call the url and obtain the full path
		$.getJSON(facebookShare, function(json){
			//create the share window
			facebookShareOpenWindow(json.url, fsid);
		});
	}
	else {
		facebookShareOpenWindow(facebookShare, fsid);
	}
}

function facebookShareOpenWindow(url, fsid) {
	//check to verify that it has not already been clicked
	if (!$('#facebook-share-'+fsid+' .facebook-share-count').hasClass('clicked')) {
		var currentCount = $('#facebook-share-'+fsid+' .facebook-share-count').text();
		if (facebookShareIsNumber(currentCount)) {
			//increment currentCount by one for being clicked
			currentCount++;
			//replace the current counter with the new click counter
			$('#facebook-share-'+fsid+' .facebook-share-count').text(currentCount);
			//add a class to prevent from multiple clicking
			$('#facebook-share-'+fsid+' .facebook-share-count').addClass('clicked');
		}
		//contact the site and initiate a reset of the facebook click to be registered
		var facebookShareReset = location.protocol+'//'+location.host+'/facebook-share/'+fsid+'/reset';
		$.get('facebook-share/'+fsid+'/reset', function(data) {});
	}
	newwindow=window.open('http://www.facebook.com/sharer/sharer.php?u='+url,'','height=333,width=620');
	if (window.focus) {newwindow.focus()}
		return false;
}

function facebookShareIsNumber (o) {
  return ! isNaN (o-0);
}

